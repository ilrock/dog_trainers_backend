json.(@user, :id, :name, :email)
if @user.bio
  json.(@user, :bio)
end

if @user.is_host
  json.events  @user.organized_events do |event|
    json.(event, :id, :title, :description, :min_guest, :max_guest, :cuisine)
  end
end

