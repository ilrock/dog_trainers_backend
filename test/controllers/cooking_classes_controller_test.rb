require 'test_helper'

class CookingClassesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @cooking_class = cooking_classes(:one)
  end

  test "should get index" do
    get cooking_classes_url, as: :json
    assert_response :success
  end

  test "should create cooking_class" do
    assert_difference('CookingClass.count') do
      post cooking_classes_url, params: { cooking_class: { cuisine: @cooking_class.cuisine, description: @cooking_class.description, host_id: @cooking_class.host_id, max_guest: @cooking_class.max_guest, min_guest: @cooking_class.min_guest, price: @cooking_class.price, title: @cooking_class.title } }, as: :json
    end

    assert_response 201
  end

  test "should show cooking_class" do
    get cooking_class_url(@cooking_class), as: :json
    assert_response :success
  end

  test "should update cooking_class" do
    patch cooking_class_url(@cooking_class), params: { cooking_class: { cuisine: @cooking_class.cuisine, description: @cooking_class.description, host_id: @cooking_class.host_id, max_guest: @cooking_class.max_guest, min_guest: @cooking_class.min_guest, price: @cooking_class.price, title: @cooking_class.title } }, as: :json
    assert_response 200
  end

  test "should destroy cooking_class" do
    assert_difference('CookingClass.count', -1) do
      delete cooking_class_url(@cooking_class), as: :json
    end

    assert_response 204
  end
end
